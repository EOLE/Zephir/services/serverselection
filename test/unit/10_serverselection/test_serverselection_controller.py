from zephir.config import ServiceConfig
from serverselection import ServerSelection
from zephir.database import connect
from psycopg2.extras import DictCursor
from util import setup_module, teardown_module


def setup_function(function):
    global serverselection, cursor, serverselectionuser
    cursor = CONN.cursor(cursor_factory=DictCursor)
    serverselectionuser = "yo"
    serverselection = ServerSelection()
    serverselection.erase_serverselection(cursor)


def test_create_serverselection():
    global serverselection, cursor, serverselectionuser
    rserverselection = serverselection.create_serverselection(cursor, "my_server_selection", "serverselection's description", serverselectionuser)
    rserverselection2 = serverselection.create_serverselection(cursor, "my_server_selection2", "second serverselection's description", serverselectionuser)
    dictuser = {serverselectionuser: 'owner'}
    assert rserverselection['serverselectionusers'] == dictuser
    assert rserverselection['dynamique'] == False
    assert rserverselection2['serverselectionid'] - rserverselection['serverselectionid'] == 1


def test_list_serverselection():
    global serverselection, cursor, serverselectionuser
    serverselection.create_serverselection(cursor, "my_server_selection", "serverselection's description", serverselectionuser)
    serverselection.create_serverselection(cursor, "my_server_selection2", "second serverselection's description", serverselectionuser)
    assert len(serverselection.list_serverselections(cursor)) == 2


def test_describe_serverselection():
    global serverselection, cursor, serverselectionuser
    rserverselection = serverselection.create_serverselection(cursor, "my_server_selection", "serverselection description", serverselectionuser)
    rserverselection2 = serverselection.describe_serverselection(cursor, rserverselection['serverselectionid'])
    dictuser = {serverselectionuser: 'owner'}
    assert rserverselection2['serverselectionname'] == 'my_server_selection'
    assert rserverselection2['serverselectiondescription'] == 'serverselection description'    
    assert rserverselection['serverselectionusers'] == dictuser
    assert rserverselection2['dynamique'] == False

def test_update_serverselection():
    global serverselection, cursor, serverselectionuser
    rserverselection = serverselection.create_serverselection(cursor, "my_server_selection", "serverselections description", serverselectionuser)
    rserverselection2 = serverselection.update_serverselection(cursor, rserverselection['serverselectionid'], "my serverselection", "my serverselections description", True, "requete")
    assert rserverselection['serverselectionid'] == rserverselection2['serverselectionid']
    assert rserverselection['serverselectionname'] != rserverselection2['serverselectionname']
    assert rserverselection['serverselectiondescription'] != rserverselection2['serverselectiondescription']
    assert rserverselection['dynamique'] != rserverselection2['dynamique']
    assert rserverselection2['dynamique'] == True
    assert rserverselection2['requete'] == "requete"

def test_delete_serverselection():
    global serverselection, cursor, serverselectionuser
    rserverselection = serverselection.create_serverselection(cursor, "my_server_selection", "serverselection's description", serverselectionuser)
    assert len(serverselection.list_serverselections(cursor)) == 1
    rserverselection1 = serverselection.delete_serverselection(cursor, rserverselection['serverselectionid'])
    assert rserverselection == rserverselection1
    assert len(serverselection.list_serverselections(cursor)) == 0

def test_add_server_to_serverselection():
    global serverselection, cursor, serverselectionuser
    rserverselection = serverselection.create_serverselection(cursor, "my_server_selection", "serverselection's description", serverselectionuser)
    rserverselection1 = serverselection.add_server_to_selection(cursor, 1, rserverselection['serverselectionid'])
    rserverselection2 = serverselection.add_server_to_selection(cursor, 2, rserverselection['serverselectionid'])
    assert rserverselection != rserverselection1
    assert rserverselection1 != rserverselection2
    assert serverselection.describe_serverselection(cursor, rserverselection['serverselectionid'])['serverselectionserversid'] == [1,2]

def test_remove_server_to_serverselection():
    global serverselection, cursor, serverselectionuser
    rserverselection = serverselection.create_serverselection(cursor, "my_server_selection", "serverselection's description", serverselectionuser)
    rserverselection = serverselection.add_server_to_selection(cursor, 1, rserverselection['serverselectionid'])
    rserverselection = serverselection.add_server_to_selection(cursor, 2, rserverselection['serverselectionid'])   
    rserverselection = serverselection.remove_server_from_selection(cursor, 1, rserverselection['serverselectionid'])
    assert serverselection.describe_serverselection(cursor, rserverselection['serverselectionid'])['serverselectionserversid'] == [2]

def test_add_user_to_serverselection():
    global serverselection, cursor, serverselectionuser
    rserverselection = serverselection.create_serverselection(cursor, "my_server_selection", "serverselection's description", serverselectionuser)
    rserverselection = serverselection.add_user_to_serverselection(cursor, rserverselection['serverselectionid'], "zephir", "admin")
    dictuser = {serverselectionuser: 'owner', 'zephir': 'admin'}
    assert serverselection.describe_serverselection(cursor, rserverselection['serverselectionid'])['serverselectionusers'] == dictuser

def test_remove_user_from_serverselection():
    global serverselection, cursor, serverselectionuser
    rserverselection = serverselection.create_serverselection(cursor, "my_server_selection", "serverselection's description", serverselectionuser)
    rserverselection = serverselection.add_user_to_serverselection(cursor, rserverselection['serverselectionid'], "zephir", "admin")
    rserverselection = serverselection.remove_user_from_serverselection(cursor, rserverselection['serverselectionid'], "zephir")
    dictuser = {serverselectionuser: 'owner'}
    assert serverselection.describe_serverselection(cursor, rserverselection['serverselectionid'])['serverselectionusers'] == dictuser

def test_update_user_from_serverselection():
    global serverselection, cursor, serverselectionuser
    rserverselection = serverselection.create_serverselection(cursor, "my_server_selection", "serverselection's description", serverselectionuser)
    rserverselection = serverselection.add_user_to_serverselection(cursor, rserverselection['serverselectionid'], "zephir", "admin")
    rserverselection = serverselection.update_user_to_serverselection(cursor, rserverselection['serverselectionid'], "zephir", "user")
    dictuser = {serverselectionuser: 'owner', 'zephir': 'user'}
    assert serverselection.describe_serverselection(cursor, rserverselection['serverselectionid'])['serverselectionusers'] == dictuser

def test_default_user_serverselection():
    global serverselection, cursor, serverselectionuser
    serverselection.create_serverselection(cursor, serverselectionuser+"_DEFAULT", "serverselection's description", serverselectionuser)
    assert serverselection.default_user_serverselection(cursor, serverselectionuser)['serverselectionname'] == serverselectionuser+"_DEFAULT"

def test_list_user_serverselections():
    global serverselection, cursor, serverselectionuser
    serverselection.create_serverselection(cursor, "my_server_selection_1", "serverselection's description", serverselectionuser)
    serverselection.create_serverselection(cursor, "my_server_selection_2", "serverselection's description", serverselectionuser)
    assert len(serverselection.list_user_serverselections(cursor, serverselectionuser)) == 2

def test_list_user_servers():
    global serverselection, cursor, serverselectionuser
    rserverselection1 = serverselection.create_serverselection(cursor, "my_server_selection", "serverselection's description", serverselectionuser)
    serverselection.add_server_to_selection(cursor, 1, rserverselection1['serverselectionid'])
    serverselection.add_server_to_selection(cursor, 2, rserverselection1['serverselectionid'])
    rserverselection2 = serverselection.create_serverselection(cursor, "other_server_selection", "serverselection's description", serverselectionuser)
    serverselection.add_server_to_selection(cursor, 3, rserverselection2['serverselectionid'])
    serverselection.add_server_to_selection(cursor, 4, rserverselection2['serverselectionid'])
    assert serverselection.list_user_servers(cursor, serverselectionuser)['serverselectionserversid'] == [2,3,4,1]
