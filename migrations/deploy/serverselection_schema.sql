-- Deploy serverselection:serverselection_schema to pg

BEGIN;

-- serverselection table creation
CREATE TABLE ServerSelection (
  ServerSelectionId SERIAL PRIMARY KEY,
  ServerSelectionName VARCHAR(255) NOT NULL,
  ServerSelectionDescription VARCHAR(255) NOT NULL,
  ServerSelectionServersId INTEGER [],
  ServerSelectionUsers hstore,
  Dynamique BOOLEAN NOT NULL,
  Requete VARCHAR(255)
);
ALTER TABLE ServerSelection ALTER COLUMN Dynamique
SET DEFAULT FALSE;

COMMIT;
