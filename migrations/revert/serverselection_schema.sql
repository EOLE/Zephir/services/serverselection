-- Revert serverselection:serverselection_schema from pg
BEGIN;

DROP SCHEMA serverselection CASCADE;

COMMIT;
