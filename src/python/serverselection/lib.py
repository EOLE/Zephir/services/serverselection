from .error import ServerSelectionError
from .query import (list_all_serverselections,                   
                   fetch_serverselection,
                   fetch_serverselection_dict,
                   fetch_all_user_serverselections,
                   fetch_default_user_serverselection,
                   fetch_all_user_servers,
                   insert_serverselection,
                   update_serverselection,               
                   delete_serverselection,
                   add_server_to_serverselection,
                   remove_server_from_serverselection,
                   add_user_to_serverselection,
                   remove_user_from_serverselection,
                   update_user_from_serverselection,
                   erase_serverselection
                   )

class ServerSelection():
    """Serverselection Manage API
    """  

    def list_serverselections(self, cursor):
        """Fetch Serverselectionss from database

        :return: list of serverselection object
        """       
        return  list_all_serverselections(cursor)

    def describe_serverselection(self, cursor, serverselectionid):
        """Get serverselection information asynchronously from database

        :param `int` serverselectionid: servserverselection identifier
        """
        return fetch_serverselection_dict(cursor, serverselectionid)
    
    def list_user_serverselections(self, cursor, serverselectionuser):
        """Get serverselections of a user

        :param str serverselectionuser: user name to be add to the serverselection
        """
        return fetch_all_user_serverselections(cursor, serverselectionuser)
    
    def list_user_servers(self, cursor, serverselectionuser):
        """Get serverselections of a user

        :param str serverselectionuser: user name to be add to the serverselection
        """
        return fetch_all_user_servers(cursor, serverselectionuser)
    
    def default_user_serverselection(self, cursor, serverselectionuser):
        """Get the default serverselection of a user

        :param str serverselectionuser: user name to be add to the serverselection
        """
        return fetch_default_user_serverselection(cursor, serverselectionuser)

    def create_serverselection(self, cursor, serverselectionname, serverselectiondescription, serverselectionuser):
        """Creates a serverselection in database

        :param str serverselectionname: serverselection name
        :param str serverselectiondescription: servermodel identifier
        :param str serverselectionuser: user name to be add to the serverselection
        :param str serverselectionuserrole: user role to be add to the serverselection
        :return: newly created serverselection identifier
        :rtype: int
        """
        return insert_serverselection(cursor, serverselectionname, serverselectiondescription, serverselectionuser)

    def update_serverselection(self, cursor, serverselectionid, serverselectionname, serverselectiondescription, dynamique, requete):
        """Updates a serverselection in database

        :param int serverselectionid: serverselection identifier
        :param str serverselectionname: server name
        :param str serverselectiondescription: serverselection description
        :param bool dynamique : is serverselection dynamique
        :param str requete: request associated to the serverselection
        :return bool: True for update success, False either
        """
        return update_serverselection(cursor, serverselectionid, serverselectionname, serverselectiondescription, dynamique, requete)

    def delete_serverselection(self, cursor, serverselectionid):
        """Deletes a serverselection in database

        :param int serverselectionid: serverselection identifier
        :return bool: True for delete success, False either
        """
        return delete_serverselection(cursor, serverselectionid)

    def erase_serverselection(self, cursor):
        erase_serverselection(cursor)

    def add_server_to_selection(self, cursor, serverselectionserversid, serverselectionid):
        """Add a server to a serverselection in database

        :param int serverselectionserversid: server identifier
        :param int serverselectionid: serverselection identifier        
        :return bool: True for addition success, False either
        """
        return add_server_to_serverselection(cursor, serverselectionserversid, serverselectionid)
    
    def remove_server_from_selection(self, cursor, serverselectionserversid, serverselectionid):
        """Remove a server from a serverselection in database

        :param int serverselectionserversid: server identifier
        :param int serverselectionid: serverselection identifier        
        :return bool: True for addition success, False either
        """
        return remove_server_from_serverselection(cursor, serverselectionserversid, serverselectionid)
    
    def add_user_to_serverselection(self, cursor, serverselectionid, username, role):
        """Add a user to a serverselection

        :param int serverselectionserversid: server identifier
        :param str username: user name to be add to the serverseleciton
        :param str role: user role to be add to the serverseleciton    
        :return bool: True for addition success, False either
        """
        return add_user_to_serverselection(cursor, serverselectionid, username, role)
    
    def remove_user_from_serverselection(self, cursor, serverselectionid, username):
        """Remove a user from a serverselection

        :param int serverselectionserversid: server identifier
        :param int serverselectionid: serverselection identifier      
        :param str username: user name to be add to the serverseleciton  
        :return bool: True for addition success, False either
        """
        return remove_user_from_serverselection(cursor, serverselectionid, username)
    
    def update_user_to_serverselection(self, cursor, serverselectionid, username, role):
        """Update a user from a serverselection

        :param int serverselectionserversid: server identifier
        :param str username: user name to be add to the serverseleciton
        :param str role: user role to be add to the serverseleciton    
        :return bool: True for addition success, False either
        """
        return update_user_from_serverselection(cursor, serverselectionid, username, role)
