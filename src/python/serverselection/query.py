from zephir.i18n import _
from json import dumps
from .error import ServerSelectionEmptyRecordDatabaseError

"""
List all serverselections
"""

FETCH_ALL_SERVERSELECTIONS = '''
    SELECT serverselectionid, serverselectionname, serverselectiondescription, serverselectionserversid, serverselectionusers, dynamique, requete
    FROM serverselection
 '''

"""
Fetch one serverselection based on its ID
"""
FETCH_SERVERSELECTION = '''
    SELECT serverselectionid, serverselectionname, serverselectiondescription, serverselectionserversid, serverselectionusers, dynamique, requete
    FROM serverselection
    WHERE serverselectionid = %s
'''

FETCH_ALL_USER_SERVERSELECTIONS = '''
    SELECT serverselectionid, serverselectionname, serverselectiondescription, serverselectionserversid, serverselectionusers, dynamique, requete
    FROM serverselection
    WHERE exist(serverselectionusers, %s)
 '''
 
FETCH_DEFAULT_USER_SERVERSELECTION = '''
    SELECT serverselectionid, serverselectionname, serverselectiondescription, serverselectionserversid, serverselectionusers, dynamique, requete
    FROM serverselection
    WHERE exist(serverselectionusers, %s) AND serverselectionname LIKE %s
 '''

"""
Creates a serverselection based on its name and serverselectionparent
"""
SERVERSELECTION_INSERT = '''
    INSERT INTO serverselection (serverselectionname, serverselectiondescription, serverselectionusers)
    VALUES (%s, %s, %s)
    RETURNING *
'''

"""
Updates a serverselection name based on its ID
"""
SERVERSELECTION_UPDATE = '''
    UPDATE serverselection
    SET serverselectionname = %s,
        serverselectiondescription = %s,
        dynamique = %s,
        requete = %s
    WHERE serverselectionid = %s
    RETURNING *
'''

"""
Deletes a serverselection based on its ID
"""
SERVERSELECTION_DELETE = '''
    DELETE
    FROM serverselection
    WHERE serverselectionid = %s
    RETURNING *
'''

"""
Add a server to a serverselection by its serverid
"""
SERVERSELECTION_ADD_SERVER = '''
    UPDATE serverselection
    SET serverselectionserversid = array_append(serverselectionserversid, %s)
    WHERE serverselectionid = %s
    RETURNING *
'''

"""
Remove a server from a serverselection
"""
SERVERSELECTION_REMOVE_SERVER = '''
    UPDATE serverselection
    SET serverselectionserversid = array_remove(serverselectionserversid, %s)
    WHERE serverselectionid = %s
    RETURNING *
'''

"""
Add User and Role to serverselection 
"""
SERVERSELECTION_ADD_USER = '''
    UPDATE serverselection
    SET serverselectionusers = serverselectionusers || %s
    WHERE serverselectionid = %s
    RETURNING *
'''

"""
Remove User from serverselection 
"""
SERVERSELECTION_REMOVE_USER = '''
    UPDATE serverselection
    SET serverselectionusers = delete(serverselectionusers, %s)
    WHERE serverselectionid = %s
    RETURNING *
'''

"""
Update User's Role from serverselection 
"""
SERVERSELECTION_UPDATE_USER = '''
    UPDATE serverselection
    SET serverselectionusers = serverselectionusers || %s
    WHERE serverselectionid = %s
    RETURNING *
'''

SERVERSELECTION_USER_SERVER_LIST = '''
    SELECT json_agg(c) AS serverselectionserversid
    FROM (
        SELECT DISTINCT unnest(serverselectionserversid)
        FROM serverselection
        WHERE exist(serverselectionusers, %s)) 
    AS dt(c)
'''

"""
Suppression du contenu de la table de serverselection
"""
SERVERSELECTION_ERASE = """DELETE from serverselection"""


def fetchone(cursor, query: str, query_parameters: tuple=None, raises: bool=False):
    cursor.execute(query, query_parameters)
    fetched = cursor.fetchone()
    if not fetched:
        if raises:
            raise ServerSelectionEmptyRecordDatabaseError(_("cannot find element with query {} and parameters {}").format(query, query_parameters))
        fetched = None
    return fetched


def serverselection_row_to_dict(serverselection):
    try:
        serverselection_obj = {'serverselectionid': serverselection['serverselectionid'],
                  'serverselectionname': serverselection['serverselectionname'],
                  'serverselectiondescription': serverselection['serverselectiondescription']}    
        if serverselection['serverselectionserversid'] is not None:
            serverselection_obj['serverselectionserversid'] = serverselection['serverselectionserversid']
        if serverselection['serverselectionusers'] is not None:
            serverselection_obj['serverselectionusers'] = parse_numeric_value_hstore(serverselection['serverselectionusers'])
        if serverselection['dynamique'] is not None:
            serverselection_obj['dynamique'] = serverselection['dynamique']
        if serverselection['requete'] is not None:
            serverselection_obj['requete'] = serverselection['requete']
    except KeyError:
        serverselection_obj = {}    
        raise(ServerSelectionEmptyRecordDatabaseError('No ServerSelection found'))        
    return serverselection_obj

def serverselection_serversid_dict(serverselection):    
    serverselection_obj = {}    
    print(dict(serverselection_obj))
    if serverselection['serverselectionserversid'] is not None:        
        serverselection_obj['serverselectionserversid'] = serverselection['serverselectionserversid']  
    return serverselection_obj
    
def list_all_serverselections(cursor):
    cursor.execute(FETCH_ALL_SERVERSELECTIONS)
    ret = []
    for serverselection in cursor.fetchall():        
        ret.append(serverselection_row_to_dict(serverselection))
    print(ret)
    return ret

def fetch_serverselection_dict(cursor, serverselectionid):   
    return serverselection_row_to_dict(fetchone(cursor, FETCH_SERVERSELECTION, (serverselectionid,), raises=True))
  

def fetch_serverselection(cursor, serverselectionid):
    fetched = fetchone(cursor, FETCH_SERVERSELECTION, (serverselectionid,))
    if fetched is None:
        raise Exception(_('unable to find a serverselection with id {}').format(serverselectionid))
    return fetched

def fetch_all_user_serverselections(cursor, serverselectionuser):
    cursor.execute(FETCH_ALL_USER_SERVERSELECTIONS, (serverselectionuser,))
    ret = []
    for serverselection in cursor.fetchall():
        ret.append(serverselection_row_to_dict(serverselection))
    return ret

def fetch_default_user_serverselection(cursor, serverselectionuser):
    userdefault = serverselectionuser+"_DEFAULT"
    try:
        fetched = fetchone(cursor, FETCH_DEFAULT_USER_SERVERSELECTION, (serverselectionuser, userdefault), raises=True)
    except ServerSelectionEmptyRecordDatabaseError:
        fetched = {}
    return serverselection_row_to_dict(fetched)

def fetch_all_user_servers(cursor, serverselectionuser):
    return serverselection_serversid_dict(fetchone(cursor, SERVERSELECTION_USER_SERVER_LIST, (serverselectionuser,), raises=True))
   

def insert_serverselection(cursor, serverselectionname, serverselectiondescription, serverselectionuser):    
    serverselectionuserrolehstore = serverselectionuser + '=>"owner"'
    return serverselection_row_to_dict(fetchone(cursor, SERVERSELECTION_INSERT, (serverselectionname, serverselectiondescription, serverselectionuserrolehstore), raises=True))

def update_serverselection(cursor, serverselectionid, serverselectionname, serverselectiondescription, dynamique, requete):
    return serverselection_row_to_dict(fetchone(cursor, SERVERSELECTION_UPDATE, (serverselectionname, serverselectiondescription, dynamique, requete, serverselectionid), raises=True))

def delete_serverselection(cursor, serverselectionid):
    return serverselection_row_to_dict(fetchone(cursor, SERVERSELECTION_DELETE, (serverselectionid,), raises=True))

def add_server_to_serverselection(cursor, serverselectionserversid, serverselectionid):
    return serverselection_row_to_dict(fetchone(cursor, SERVERSELECTION_ADD_SERVER, (serverselectionserversid, serverselectionid), raises=True))

def remove_server_from_serverselection(cursor, serverselectionserversid, serverselectionid):
    return serverselection_row_to_dict(fetchone(cursor, SERVERSELECTION_REMOVE_SERVER, (serverselectionserversid, serverselectionid), raises=True))

def add_user_to_serverselection(cursor, serverselectionid, username, role):
    serverselectionuserrolehstore = username + '=>' + role
    return serverselection_row_to_dict(fetchone(cursor, SERVERSELECTION_ADD_USER, (serverselectionuserrolehstore, serverselectionid), raises=True))

def remove_user_from_serverselection(cursor, serverselectionid, username):
    return serverselection_row_to_dict(fetchone(cursor, SERVERSELECTION_REMOVE_USER, (username, serverselectionid), raises=True))

def update_user_from_serverselection(cursor, serverselectionid, username, role):
    serverselectionuserrolehstore = username + '=>' + role
    return serverselection_row_to_dict(fetchone(cursor, SERVERSELECTION_UPDATE_USER, (serverselectionuserrolehstore, serverselectionid), raises=True))

def erase_serverselection(cursor):
    """
    Supprime le contenu de la table Serverselection
    """
    cursor.execute(SERVERSELECTION_ERASE)


def parse_numeric_value_hstore(string):
    if not string:
        return {}
    tmp = string.replace('"',"").split(",")
    res = {}
    for i in tmp:
        key, value = map(str.strip, i.split("=>"))
        res[key] = value
    return res


def dict_to_hstore_text(dic):
    res = ''
    for key, value in dic.items():
        res += '"{}"=>"{}",'.format(key, value)
    if res:
        res = res[:-1]
    return res
