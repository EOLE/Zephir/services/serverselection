class ServerSelectionError(Exception):
    """Base class of :class:`ServerSelection` exceptions
    """

class ServerSelectionErrorDatabaseNotAvailable(ServerSelectionError):
    """No available database service
    """


class ServerSelectionErrorDbConnection(ServerSelectionError):
    """Database connection error
    """


class ServerSelectionErrorInvalidServerSelectionId(ServerSelectionError):
    """Invalid serverselection ID
    """

class ServerSelectionErrorServerSelectionNameNotProvided(ServerSelectionError):
    """Serverselectionname expected
    """


class ServerSelectionErrorUnknownServerSelectionId(ServerSelectionError):
    """Unknown serverselection ID
    """

class ServerSelectionErrorDuplicateServerSelectionName(ServerSelectionError):
    """Duplicate serverselection name 
    """
class ServerSelectionEmptyRecordDatabaseError(ServerSelectionError):
    """Empty value in serverselection 
    """

